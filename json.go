package main

import (
	"encoding/json"
	"fmt"
	MQTT "git.eclipse.org/gitroot/paho/org.eclipse.paho.mqtt.golang.git"
	"os"
	"time"
)

type Data struct {
	Job   int
	Name  string
	Value int
}

var data = Data{Job: 1, Name: "เติมเงินบัตร", Value: 100}

// var mock = []Data{
// 	{Job: 1, Name: "เติมเงินบัตร", Value: 100},
// 	{Job: 1, Name: "เติมเงินบัตร", Value: 200},
// 	{Job: 2, Name: "คืนเงินบัตร", Value: 50},
// }

//define a function for the default message handler
var f MQTT.MessageHandler = func(client *MQTT.Client, msg MQTT.Message) {
	fmt.Printf("TOPIC: %s\n", msg.Topic())
	fmt.Printf("MSG: %s\n", msg.Payload())
}

var (
	broker = "tcp://nava.work:1883"
)

func main() {
	opts := MQTT.NewClientOptions().AddBroker(broker)
	opts.SetClientID("go-test")
	opts.SetDefaultPublishHandler(f)

	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	balance := 0
	for i := 0; i < 1000; i++ {
		if i%2 == 0 {
			data.Job = 2
			data.Name = "คืนเงินบัตร"
			data.Value = -5
			balance += data.Value
		} else {
			data.Job = 1
			data.Name = "เติมเงินบัตร"
			data.Value = i + 10
			balance += data.Value
		}
		json, err := json.Marshal(data)
		if err != nil {
			fmt.Println("JSON Encoding Error", err)
		}
		token := c.Publish("pb/1/1/1", 0, false, json)
		if token.Error() != nil {
			fmt.Println("Token Publish Error...", token.Error())
		}
		fmt.Printf("Loop: %d json == %v Balance == %v\n", i, string(json), balance)
		token.Wait()
		time.Sleep(3 * time.Second)
	}
	sub(broker)
	//unsubscribe from /go-mqtt/sample
	// if token := c.Unsubscribe("pb/+/+/1"); token.Wait() && token.Error() != nil {
	// 	fmt.Println(token.Error())
	// 	os.Exit(1)
	// }

	c.Disconnect(250)
}

func sub(b string) {
	opts := MQTT.NewClientOptions().AddBroker(b)
	opts.SetClientID("go-test")
	opts.SetDefaultPublishHandler(f)

	c := MQTT.NewClient(opts)
	token := c.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
		// return token.Error()
	}
	token = c.Subscribe("pb/+/+/1", 0, nil)
	// callback := MQTT.MessageHandler
	// token = c.Subscribe("pb/+/+/1", 0, &callback)

	if token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		// return token.Error()
		os.Exit(1)
	}
	// c.Subscribe(topic string, qos byte, callback mqtt.MessageHandler)
	fmt.Println(data)
	// return token.Error()
}
