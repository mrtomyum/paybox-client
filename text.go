package mq

import (
	// "encoding/json"
	"fmt"
	MQTT "git.eclipse.org/gitroot/paho/org.eclipse.paho.mqtt.golang.git"
	"os"
	"time"
)

type Data struct {
	Job   int
	Name  string
	Value int
}

// var mock = []Data{
// 	{Job: 1, Name: "เติมเงินบัตร", Value: 100},
// 	{Job: 1, Name: "เติมเงินบัตร", Value: 200},
// 	{Job: 2, Name: "คืนเงินบัตร", Value: 50},
// }

//define a function for the default message handler
var f MQTT.MessageHandler = func(client *MQTT.Client, msg MQTT.Message) {
	fmt.Printf("TOPIC: %s\n", msg.Topic())
	fmt.Printf("MSG: %s\n", msg.Payload())
}

func main() {
	opts := MQTT.NewClientOptions().AddBroker("tcp://nava.work:1883")
	opts.SetClientID("go-test")
	opts.SetDefaultPublishHandler(f)

	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	//
	// if token := c.Subscribe("pb/+/+/1", 0, nil); token.Wait() && token.Error() != nil {
	// 	fmt.Println(token.Error())
	// 	os.Exit(1)
	// }

	//Publish 5 messages to /go-mqtt/sample at qos 1 and wait for the receipt
	//from the server after sending each message
	for i := 0; i < 1000; i++ {
		text := fmt.Sprintf("สวัสดีครับ นี่คือข้อความที่ #%d!", i)
		token := c.Publish("pb/1/1/1", 0, false, text)
		if token.Error() != nil {
			fmt.Println("Token Publish Error...", token.Error())
		}
		fmt.Printf("Loop: %d MockData == %v\n", i, text)
		token.Wait()
		time.Sleep(3 * time.Second)

	}
	//unsubscribe from /go-mqtt/sample
	if token := c.Unsubscribe("pb/+/+/1"); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}
	c.Disconnect(250)
}
